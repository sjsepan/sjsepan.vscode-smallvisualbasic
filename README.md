# Small Visual Basic (sVB) for VS Code

This Visual Studio Code extension is a fork of [**@alxnull's extension**](https://marketplace.visualstudio.com/items?itemName=alxnull.vscode-smallbasic) supporting [**Microsoft's Small Basic**](http://smallbasic.com) language; it instead supports [**Small Visual Basic (sVB)**](https://marketplace.visualstudio.com/items?itemName=ModernVBNET.sVBInstaller), a derivative of Small Basic.

## Features

- Syntax highlighting extended from Small Basic to Small Visual Basic
    (e.g. `ForEach...In`, `Function...EndFunction`, `Return`, `Next`, `Wend`, `ExitLoop`, `ContinueLoop`)
- Code snippets extended from Small Basic to Small Visual Basic
    (e.g. `For ... Next`, `While...Wend`, `If...ElseIf...`, `ForEach...In...`, `Function..EndFunction`)

## Installation

### From repository

Download the .vsix file and choose 'Install from VSIX...' from the app.

## Changelog

See the [changelog](https://gitlab.com/sjsepan/vscode-smallvisualbasic/blob/HEAD/CHANGELOG.md) for details.

## Issues

-The Folding feature, as inherited from the Microsoft Small Basic extension by @alxnull, did not have a Function/EndFunction pair as Small Visual Basic does, so while folding a Sub hides everything afterward, including the EndSub, folding a Function only hides what is in between. This is because the fall-back and default behavior for text not targeted by the enhanced folding is Indentation-based folding. Since the JSON scheme for the enhanced folding does not allow for multiple target pairs, and since the default folding is adequate, I do not see this as a problem that needs to be addressed. (If anything, I could see removing the enhanced folding for Sub/EndSub, but will not do so unless there is a demand for it.)

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/16/2025
